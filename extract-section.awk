# Example usage by passing in <headerSearch> to identify the target header in
# <testFile>.
# Returns the matching section from the listed file.
# awk -v headerSearch=<headerSearch> -f extract-section.awk <testFile>
BEGIN{
	headerRegex = ".*"headerSearch".*";
	headerMatch = 0;
	inSection = 0;
	contentStarted = 0;
	earlyExit = 0;

	line3 = "";
	line2 = "";
	line1 = "";
	line0 = "";

	# Where we are looking for the following termination pattern:
	# 3 last line in the section
	# 2 <empty newline>
	# 1 Next section header
	# 0 <next section header underline>
}

{
	# Search for the header if we aren't in a section.
	if(!inSection && match($0, headerRegex)){
		headerMatch = 1;
		line0 = $0;
	}

	# If we are already in a section, don't check for the header start.
	# If we match the header, check ahead for the heading underline to confirm
	# we are in a section.
	if(!inSection && headerMatch){
		if(match($0, /^#+$/)){
			inSection = 1;
			headerType = "#"
		}else if(match($0, /^\*+$/)){
			inSection = 1;
			headerType = "*"
		}else if(match($0, /^=+$/)){
			inSection = 1;
			headerType = "="
		}else{
			inSection = 0;
		}
		line1 = line0;
		line0 = $0;
	}else if(inSection){
		if(!contentStarted && !match(line3, /^$/)){
			contentStarted = 1;
			print line3;
		}else if (contentStarted){
			print line3;
		}

		# If we are in a section, check ahead for the next header.
		endSectionRegex = "^\\"headerType"+$";
		if(match($0, endSectionRegex)){
			print line2;
			print line1;
			earlyExit = 1;
			exit;
		}

		# Increment the line counters.
		line3 = line2;
		line2 = line1;
		line1 = line0;
		line0 = $0;
	}
}

END{
	if(!earlyExit){
		# Print to the end of the file.
		print line3;
		print line2;
		print line1;
		print line0;
	}
}
