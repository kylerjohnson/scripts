BEGIN{
	FS = "-";
	inPending = 0;
	pendingIndent = 0;
}

/- p/ {
	print;
	inPending = 1;
	pendingIndent = length($1);
	next;
}

{
	if(!inPending){
		# check for header
		if(match($0, /^#+$/)){
			header = 1;
		}else if(match($0, /^\*+$/)){
			header = 1;
		}else{
			header = 0;
		}

		if(header){
			print previousLine
			print $0"\n"
			header = 0;
		}
	}else if(inPending){
		# check if continuing bullet
		if(length($0) == 0){
			print
		}else if(length($1) > pendingIndent){
			print
		}else{
			inPending = 0;
			pendingIndent = 0;
		}
	}
	previousLine = $0;
}

END{
print FILENAME;
}
